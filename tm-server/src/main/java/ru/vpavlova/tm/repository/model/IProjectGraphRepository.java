package ru.vpavlova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vpavlova.tm.api.repository.IGraphRepository;
import ru.vpavlova.tm.entity.ProjectGraph;

import java.util.List;
import java.util.Optional;

public interface IProjectGraphRepository extends IGraphRepository<ProjectGraph> {

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<ProjectGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<ProjectGraph> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    Optional<ProjectGraph> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

}
