package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.IPropertyService;
import ru.vpavlova.tm.repository.dto.IUserRepository;
import ru.vpavlova.tm.api.service.dto.IUserService;
import ru.vpavlova.tm.dto.User;
import ru.vpavlova.tm.enumerated.Role;
import ru.vpavlova.tm.exception.empty.*;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.UserNotFoundException;
import ru.vpavlova.tm.exception.user.LoginExistsException;
import ru.vpavlova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @Nullable
    @Autowired
    public IUserRepository userRepository;

    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserRepository getRepository() {
        return userRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final User user
    ) {
        if (user == null) throw new ObjectNotFoundException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login, @Nullable final String password
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<User> findByLogin(
            @Nullable final String login
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean isLoginExist(
            @Nullable final String login
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findByLogin(login).isPresent();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLocked(true);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.removeByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.save(user.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Optional<User> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final User user = userOptional.get();
        user.setLocked(false);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Optional<User> user = findOneById(userId);
        if (!user.isPresent()) throw new ObjectNotFoundException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.save(user.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final IUserRepository userRepository = getRepository();
        entities.forEach(userRepository::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.deleteById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<User> findAll() {
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<User> findOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getRepository();
        return userRepository.findById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getRepository();
        userRepository.deleteById(id);
    }

}
