package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.IPropertyService;
import ru.vpavlova.tm.repository.model.ISessionGraphRepository;
import ru.vpavlova.tm.api.service.model.ISessionGraphService;
import ru.vpavlova.tm.api.service.model.IUserGraphService;
import ru.vpavlova.tm.entity.SessionGraph;
import ru.vpavlova.tm.entity.UserGraph;
import ru.vpavlova.tm.enumerated.Role;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.user.AccessDeniedException;
import ru.vpavlova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
public final class SessionGraphService extends AbstractGraphService<SessionGraph> implements ISessionGraphService {

    @NotNull
    @Autowired
    public ISessionGraphRepository sessionGraphRepository;

    @NotNull
    public ISessionGraphRepository getRepository() {
        return sessionGraphRepository;
    }

    @NotNull
    @Autowired
    private IUserGraphService userGraphService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new ObjectNotFoundException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionRepository.save(session);
    }

    @Override
    public void addAll(@NotNull List<SessionGraph> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionRepository.saveAll(entities);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<SessionGraph> findAll() {
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        return sessionRepository.findAll();
    }

    @NotNull
    @Override
    public Optional<SessionGraph> findById(
            @Nullable String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        return Optional.of(sessionRepository.getOne(id));
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionRepository.deleteById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @NotNull final Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user.get());
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionRepository.save(signSession);
        return signSession;
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        if (!sessionRepository.findById(session.getId()).isPresent()) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if ((session.getUser().getId()).isEmpty()) throw new AccessDeniedException();
        validate(session);
        @NotNull final Optional<UserGraph> user = userGraphService.findById(session.getUser().getId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionGraph close(@Nullable SessionGraph session) {
        if (session == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionGraphRepository.deleteById(session.getId());
        return session;
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        @NotNull final Optional<UserGraph> user = userGraphService.findByLogin(login);
        if (!user.isPresent()) return false;
        if (user.get().isLocked()) throw new AccessDeniedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (password.isEmpty()) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionGraph entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ISessionGraphRepository sessionRepository = getRepository();
        sessionGraphRepository.deleteById(entity.getId());
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
