package ru.vpavlova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vpavlova.tm.api.endpoint.IProjectEndpoint;
import ru.vpavlova.tm.api.service.IProjectService;
import ru.vpavlova.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        projectService.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Project save(
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        projectService.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Project> saveAll(
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.clear();
    }

}
