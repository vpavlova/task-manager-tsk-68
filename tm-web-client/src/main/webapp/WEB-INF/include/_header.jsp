<html>
<head>
    <title>${view_name}</title>
    <style>
        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: cornflowerblue;
            border: black solid 1px;
            padding: 10px;
        }
    </style>
</head>
<body>
<h1>TASK MANAGER</h1>
<a href="/projects">Projects</a>
<a href="/tasks">Tasks</a>
