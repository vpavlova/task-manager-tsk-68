package ru.vpavlova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractProjectListener;
import ru.vpavlova.tm.endpoint.Project;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.ProjectNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class ProjectByIndexViewListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    @EventListener(condition = "@projectByIndexViewListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = projectEndpoint.findProjectByIndex(session, index);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

}
